\ProvidesPackage{www_math_commands}
\RequirePackage{mathtools}

% Define Options
\newif\if@geometry
\newif\if@standardsets
\newif\if@slashed
\newif\if@norms
\newif\if@errorterms
\newif\if@pde
\newif\if@miscops
\newif\if@misclabeled
\newif\if@roman
\newif\if@convex

\DeclareOption{geom}{\@geometrytrue \typeout{Loaded geometry commands}}
\DeclareOption{slash}{\@slashedtrue \typeout{Enabled slashed commands}}
\DeclareOption{sets}{\@standardsetstrue \typeout{Additional sets and set-builder are enabled}}
\DeclareOption{norm}{\@normstrue \typeout{Loaded norm-like commands}}
\DeclareOption{errorterm}{\@errortermstrue \typeout{Enabled automatic error terms}}
\DeclareOption{pde}{\@pdetrue \typeout{Loaded PDE and functional analysis commands}}
\DeclareOption{miscops}{\@miscopstrue \typeout{Loaded miscellaneous operators}}
\DeclareOption{labeled}{\@misclabeledtrue \typeout{Loaded miscellaneous labeled objects}}
\DeclareOption{roman}{\@romantrue \typeout{Enabled Roman numerals for term numbering}}
\DeclareOption{conv}{\@convextrue \typeout{Enabled convex analysis commands}}

\ProcessOptions\relax

%%%%% Math commands definitions %%%%%
% Some always-defined objects due to their common use.
\newcommand*\eqdef{\overset{\mbox{\tiny{def}}}{=}}
\newcommand*\suchthat{\ensuremath{\;\textrm{s.t.}\;}}
\newcommand*\pholder{\;{\text{-}}\;}  % Place holder symbol for expressions like 
    %  E(t) = \pnorm{f(t,\pholder)}{L^2(\Real)}
    % Previous version:
    % \newcommand*\pholder[1][\bullet]{\mathord{#1}}
    % Configurable to use other symbols, such as \pholder[-] 
\newcommand*\Real{\mathbb{R}}
\newcommand*\Complex{\mathbb{C}}
\newcommand*\Integer{\mathbb{Z}}
\newcommand*\Natural{\mathbb{N}}
\newcommand*\Id{\mathbf{1}}
% The starred version \D* adds no space correction, and is meant to be
% used for differential forms and expressions like d/dt. The unstarred
% version is meant to be used for the volume measure in integrals to
% get better spacing.
\newcommand*\D{\@ifstar\D@Star\D@noStar}
\newcommand*\D@noStar[1][]{~\mathrm{d}#1}
\newcommand*\D@Star[1][]{\mathrm{d}#1}
% The \superbig macro scales a standard delimiter to a user-specified height
% that is provided as the first argument. The star version has zero height.
% This is useful for use to brace, for example, multiple rows of a single align 
% environment. The optional argument allows the braces to be shifted vertically 
% for adjustment.
% The plain variant ends up being a `\mathord` the ..l and ..r variants make them 
% \mathopen and \mathclose respectively
\newcommand*\superbig@nostar[3][0pt]{\raisebox{#1}{$\left.\rule{0pt}{#2}\!\right#3$}}
\newcommand*\superbig@star[3][0pt]{\raisebox{#1}[-#1][-#1]{$\left.\rule{0pt}{#2}\!\right#3$}}
\newcommand*\superbig{\@ifstar\superbig@star\superbig@nostar}
\newcommand*\superbigl{\mathopen\bgroup\aftergroup\egroup\superbig}
\newcommand*\superbigr{\mathclose\bgroup\aftergroup\egroup\superbig}

% Standard differential/semi-Riemannian geometry commands
\if@geometry
\RequirePackage{tensor}
\newcommand*{\covD}{\nabla}
\newcommand*{\lieD}{\mathcal{L}}
\newcommand*{\codiff}{\delta}
\newcommand*{\intprod}{\iota}
\newcommand*{\Riem}[1][]{\tensor{\mathrm{Riem}}{#1}}
\newcommand*{\Ricci}[1][]{\tensor{\mathrm{Ric}}{#1}}
\newcommand*{\Deform}[3][0]{\tensor[^{(#2,#1)}]{\pi}{#3}}
\newcommand*{\Chris}[1]{\tensor{\Gamma}{#1}}
\newcommand*\ub@nostar[1]{\vphantom{#1}\smash{\underline{#1}}}
\newcommand*\ub@star[1]{\underline{#1}}
\newcommand*\ub{\@ifstar\ub@star\ub@nostar}  % The normal version has some vertical space fixes. But this may end up
                  % with the symbol too cramped in some situations. The starred version undoes the vertical space fix. 
\newcommand*\bar@nostar[1]{\vphantom{#1}\smash{\overline{#1}}}
\newcommand*\bar@star[1]{\overline{#1}} 
\renewcommand*\bar{\@ifstar\bar@star\bar@nostar} % Redefine bar to match ub
\newcommand*\Scri{\mathscr{I}}
\newcommand*{\Dvol}{\mathrm{dvol}}

% Geometry stuff using slashed
\if@slashed
\RequirePackage{slashed}
\declareslashed{}{/}{.05}{.1}{\nabla} 
%See slashed.sty for the syntax for above. The third and fourth
%parameters are for horizontal and vertical adjustments of the slash
\newcommand*{\subD}{\slashed\nabla}
\declareslashed{}{/}{0}{-.1}{\triangle}
\newcommand*{\subDD}{\slashed\triangle}
\fi
\fi

% Error terms (rewritten to not use the broken 'constants' package)
\if@errorterms
\newcounter{errorterm@cntr}
\newcommand{\errs}{\refstepcounter{errorterm@cntr}\ensuremath{\mathfrak{e}_\theerrorterm@cntr}}
\newcommand{\errslab}[1]{\errs\label{#1}}
\newcommand{\errsref}[1]{\ensuremath{\mathfrak{e}_{\ref{#1}}}}
\fi

% Basic norm type objects
\if@norms
\DeclarePairedDelimiterXPP\www@innerprod[3]{}\langle\rangle{_{#1}}{#2,#3}
\newcommand*{\innerprod}[3][]{\www@innerprod*{#1}{#2}{#3}}
\DeclarePairedDelimiterXPP\www@abs[2]{}\lvert\rvert{_{#1}}{#2}
\newcommand*\abs@nostar[2][]{\www@abs*{#1}{#2}} % The standard version does automatic sizing
\newcommand*\abs@star[3][]{\www@abs[#3]{#1}{#2}} % The starred version requires manual size selection as the third argument
\newcommand*\abs{\@ifstar\abs@star\abs@nostar}
\DeclarePairedDelimiterXPP\www@norm[2]{}\lVert\rVert{_{#1}}{#2}
\newcommand*\norm@nostar[2][]{\www@norm*{#1}{#2}}
\newcommand*\norm@star[3][]{\www@norm[#3]{#1}{#2}}
\newcommand*\norm{\@ifstar\norm@star\norm@nostar}
\fi

% Additional sets and set notation
\if@standardsets
\DeclarePairedDelimiterX\set[2]\lbrace\rbrace{#1\;\delimsize\vert\;#2}
\newcommand*\Rational{\mathbb{Q}}
\newcommand*\Sphere{\mathbb{S}}
\newcommand*\Torus{\mathbb{T}}
\newcommand*\Ball{\mathbb{B}}
\newcommand*\RProj{\mathbb{RP}} 
\newcommand*\CProj{\mathbb{CP}}
\fi

% PDE and functional analysis commands
\if@pde
\newcommand*\weakto{\underset{\mbox{\tiny{weak}}}{\to}}
\newcommand*\Sobolev{\@ifstar\Sobolev@Star\Sobolev@noStar}
\newcommand*\Sobolev@noStar[2]{W^{#1,#2}}
\newcommand*\Sobolev@Star[2]{\mathring{W}^{#1,#2}}
\newcommand*\sobolev{\@ifstar\sobolev@Star\sobolev@noStar}
\newcommand*\sobolev@noStar[1]{H^{#1}}
\newcommand*\sobolev@Star[1]{\mathring{H}^{#1}}
\newcommand*\Schwartz{\mathscr{S}}
\DeclarePairedDelimiter\jb\langle\rangle % Japanese Bracket
\DeclareMathOperator{\Fourier}{\mathscr{F}} % Fourier transform symbol
\newcommand*\FT{\@ifstar\FT@star\FT@nostar} 
\newcommand*\FT@nostar[1]{\widehat{#1}} % Fourier transform with hat
\newcommand*\FT@star[1]{\widetilde{#1}} % Fourier transform with tilde (starred version)
\fi

% Convex analysis commands
\if@convex
\newcommand*\lnsgm[2][]{\overline{#2}^{#1}}
\DeclareMathOperator{\hull}{conv} % Convex Hull operator
\fi

% Miscellaneous operators
\if@miscops
\DeclareMathOperator{\trace}{tr}
\DeclareMathOperator{\proj}{proj}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\diam}{diam}
\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\sign}{sgn}
\fi

% Various labelled stuff
\if@misclabeled
\newcommand*\morph[1]{\overset{\mbox{\tiny #1}}{\cong}}
\newcommand*\fnto[1]{\overset{#1}{\to}}
\fi

%------Start Roman numeral commands
\if@roman
\newcommand*{\rmnum}[1]{\romannumeral #1}
\newcommand*{\Rmnum}[1]{\expandafter\@slowromancap\romannumeral #1@}
\newcommand*{\term}[1]{\ensuremath{\mathit{\Rmnum{#1}}}}
  %This command is for splitting an expression into sub-parts. Using
  %this formulation the spacing is corrected. 
\fi
%------End Roman numeral commands
%%%%% End Math commands defs %%%%%
