# www-textools

A collection of TeX-related things. 

## Newer Stuff

All my newer stuff I organized following the TDS in the `texmf` directory in this repository. The easiest way to use is to clone this repository and create symlinks to these from your `$TEXMFHOME` (usually `~/texmf` on Linux systems). 

* [wwwslides.cls](texmf/tex/latex/wwwslides/) is a document class for writing slide presentations; it is designed to be _minimal_ (so none of those `beamer` bells and whistles), and themed to be appropriate for Michigan State University. 
* [wwwmath.sty](texmf/tex/latex/wwwmath/) is my personal math macros for paper writing. It replaces the `www_math_commands.sty` file found elsewhere in this repository. 
* [wwwsymb.sty](texmf/tex/latex/wwwsymbs/) is a few whimsical symbols useful for textbooks. 
* [wwwfontselect.sty](texmf/tex/latex/wwwfontselect/) exists mainly so I don't have to remember the package names for various font packages that I like.
* [msumathletter.cls](texmf/tex/latex/msumathletter/) contains my recreated letterhead for MSU Math Department
* Other class files are available in [wwwcls]{texmf/tex/latex/wwwcls/)

_Note to self_:  
* Use `git tag -a <file>-<version> -m "message"` to create release tags. Use `git push --follow-tags` to push them.

## Older Stuff

Everything now in the "Newer Stuff" section is considered to be Older Stuff. (Transition around 2023.)

To use: download the relevant class (`.cls`) and package (`.sty`) files. You may need to read the source to see what are the dependencies. 

I provide some pre-configured TeX templates (`.tex-template`). 


## Copyright

A few of the files in this repo are from third parties (notably `ocg-p.sty` and `ocgx.sty`); those are distributed under the license specified in the source-code or subdirectory README. 

Everything else is released in the public domain.


