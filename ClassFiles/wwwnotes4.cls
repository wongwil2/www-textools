%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  wwwnotes4 class by Willie W-Y Wong     
%%%%  For usage see template file
%%%%
%%%%  Extended referencing commands:
%%%%    Using cleveref
%%%%  Descriptive labelling commands:
%%%%    \desclabel, \descref, \desceqref
%%%%  Margin note command:
%%%%    \marginnote
%%%%    Note: there is a bug in the marginfix package where sometimes when a marginpar 
%%%%      is called near a page break, that the marginpar gets silently dropped. A temp
%%%%      fix is to insert \clearmargin right before the note in question, so that the 
%%%%      marginpar buffer used by marginfix is cleared and the new marginpar is "forced 
%%%%      unto the next page" per se, with the "next page" being usually the correct page. 
%%%%  One should only use \chapter and \section, and optionally \part for 
%%%%    sectioning. Don't use \subsection! That's too many levels down, as 
%%%%    Tufte would say. 


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{wwwnotes4}
	[2021/04/16 v0.9 Willie Wai Yeung Wong Notes class]

\LoadClass[10pt,titlepage,leqno,openright,twoside]{report}

\RequirePackage[T1]{fontenc}
\RequirePackage[english]{babel}

\RequirePackage{ifthen}

\RequirePackage{geometry}
\RequirePackage[pagestyles]{titlesec}
\RequirePackage{etoolbox}
\RequirePackage{refcount}
\RequirePackage{marginfix}
% marginfix sometimes loses notes completely? WTF?
\RequirePackage{titling}
\RequirePackage{moresize}

\RequirePackage{enumitem}
\RequirePackage{imakeidx}
\RequirePackage[backend=biber,style=reading, entrykey=false, file=false,library=false, abstract=false]{biblatex}

\RequirePackage[oldstylenums,narrowiints,intlimits,sumlimits,noDcommand,leqno]{kpfonts}
\RequirePackage[protrusion,babel]{microtype}

\RequirePackage[leqno]{mathtools, empheq}
\RequirePackage{amsthm}
\RequirePackage{thmtools}

\RequirePackage[colorlinks=false]{hyperref}
\RequirePackage{cleveref}

%% Geometry (geometry)
\geometry{papersize={7.4in,9.5in},includemp=true,marginparwidth=1.44in,width=6.1in,bindingoffset=0.2in,twoside,hmarginratio={2:1},vmarginratio={3:4},headheight=14pt,headsep=0.2in,footskip=0.5in}

%% Git backup (gracefully degrades if user doesn't use Git)
\def\gitShortHash{(git not used)}
\def\GitPageFooter{(version info not available)}
\@input{\jobname.gitinfo}  % Inputs the .gitinfo file for the main document
\newcommand*\stashGitInfo{\let\old@gitSH\gitShortHash \let\old@GitPF\GitPageFooter}
\newcommand*\wwwinclude[1]{\begingroup\@input{#1.gitinfo}\include{#1}\endgroup}
\newcommand*\wwwinput[1]{\begingroup\@input{#1.gitinfo}\input{#1}\endgroup}


%% Title Page (titling, moresize)[hyperref]
\newcommand*\thesubtitle{}
\newcommand*\subtitle[1]{\renewcommand*\thesubtitle{#1}}
\let\oldtitle\title
\newcommand*\thetitletext{}
\renewcommand*\title[1]{\renewcommand*\thetitletext{#1}\oldtitle{#1}}
\pretitle{\begin{flushleft}\HUGE \bfseries \scshape}
\posttitle{\par\end{flushleft}\begin{flushleft}\Large \thesubtitle\end{flushleft} \vfill}
\preauthor{\begin{flushright}\LARGE}
\postauthor{\par\end{flushright}%
	\hypersetup{pdfauthor={\theauthor},pdftitle={\thetitle},%
	pdfsubject={Mathematics}}%
	\vskip 1.5in}
\predate{\begin{center}\footnotesize Revision based on commit \texttt{\gitShortHash{}} as of }
\postdate{\par\end{center}}
\setlength{\droptitle}{0.5in}

%% Headers and Footers (titlesec, moresize)
\widenhead*{0.1in}{0.45in}
\newpagestyle{wwwfancy}{\headrule%
	\sethead[\thepage]%
		[][\scriptsize\slshape\thetitletext]%
		{\scriptsize\slshape\ifthechapter{\chaptertitlename~\thechapter.~\chaptertitle}{}}{}%
		{\thepage}%
	\setfoot{\tiny \copyright~\theauthor}{}{\tiny \GitPageFooter}}
\pagestyle{wwwfancy}

%Bibliography page style  (biblatex)
\defbibheading{bibintoc}[\bibname]{%
	\chapter*{#1}%
	\addcontentsline{toc}{chapter}{#1}
	\markboth{}{}
}
\newpagestyle{wwwbibfancy}{\headrule%
	\sethead[\thepage]%
		[][\slshape\thetitletext]%
		{\slshape \bibname}{}%
		{\thepage}%
	\setfoot{\tiny \copyright~\theauthor}{}{\tiny \GitPageFooter}}


% To make the clear page completely empty, with no header or footer
\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else\hbox{}
	%\vspace*{\fill}
	%\begin{center}
	%   This page intentially blank.
	%\end{center}
	%\vspace{\fill}
	\thispagestyle{empty}
	\newpage
	\if@twocolumn\hbox{}\newpage\fi\fi\fi}

%% Section/Chapter titles (titlesec)
\setcounter{secnumdepth}{0}
\setcounter{tocdepth}{1}
\titleformat{\part}[display]{\bfseries\huge}%
	{\filcenter\partname\ \thepart}{0.4in}%
	{\filcenter\Huge}[\thispagestyle{empty}]
\titleformat{\chapter}[display]{\bfseries\Large}%
	{\filleft\chaptertitlename\ {\huge\thechapter}}%
	{1ex}{\titlerule\vspace{2ex}\filcenter\Huge}%
	[\vspace{1ex}\titlerule]
\titleformat{\section}{\bfseries\Large}{}{}{}
%%%%% End basic set-up  %%%%%

%%%%% Theorem like environments (amsthm) %%%%%
\numberwithin{equation}{chapter}

\declaretheoremstyle[
headfont=\normalfont\scshape\bfseries,
headformat=swapnumber,
headindent=0pt,
notefont=\normalfont\bfseries,
bodyfont=\normalfont,
postheadspace=\newline,
headpunct={},
qed=\ensuremath{\blacksquare},
sibling=equation
]{wwwthmmain}

\declaretheorem[style=wwwthmmain]{theorem, proposition, lemma, corollary, definition}

\declaretheoremstyle[
headfont=\normalfont\scshape\bfseries,
headformat=swapnumber,
headindent=0pt,
notefont=\normalfont\bfseries,
bodyfont=\normalfont\itshape,
postheadspace=\newline,
headpunct={},
qed=\ensuremath{\clubsuit},
sibling=equation
]{wwwthmconj}

\declaretheorem[style=wwwthmconj,name=Open Question]{openquestion}
\declaretheorem[style=wwwthmconj]{conjecture}

\declaretheoremstyle[
headfont=\normalfont\scshape\bfseries,
headformat=swapnumber,
headindent=0pt,
notefont=\normalfont\bfseries,
bodyfont=\normalfont,
postheadspace=\newline,
headpunct={},
qed=\ensuremath{\diamondsuit},
sibling=equation
]{wwwthmexa}

\declaretheorem[style=wwwthmexa]{example, exercise}

\newenvironment{pfof}[1]%
	{\begin{proof}[Proof of \Cref{#1}]}%
	{\end{proof}}

\newenvironment{proofsketch}{\renewcommand*\proofname{Sketch of proof}\begin{proof}}{\end{proof}}

\declaretheoremstyle[
headfont=\normalfont\bfseries,
headformat=\NUMBER\NOTE,
headindent=0pt,
notefont=\normalfont\bfseries,
bodyfont=\normalfont,
postheadspace=1em,
headpunct={},
sibling=equation
]{wwwpara}

\declaretheorem[style=wwwpara,name=\P]{thot}
\declaretheorem[style=wwwpara]{convention, remark}
%%%%% End Theorem like environments %%%%%

%%%%% Margin Notes and Descriptive Labelling (refcount, ntheorem, amsmath) %%%%%
\newcommand*\marginfont{\footnotesize\itshape}
\setlength\marginparpush{9pt}
\newcommand{\marginnote}[1]{%
	\marginpar[\raggedleft\marginfont #1]{\raggedright\marginfont #1}}
\newcommand{\margincite}[1]{\marginnote{\cite{#1}}}

% The call to ifmeasuring below is due to AMS-math environments being called twice, first time to measure, the second time to print
% See e.g. http://tex.stackexchange.com/a/59088/119
% Store descriptions in the aux files so is robust against \includes
\newcommand\wwwnotes@dlabel@store[2]{\global\@namedef{www@label@store@description@#1}{#2}}

\newcommand\desclabel[2]{%
	\label{#1}%
	\ifmeasuring@
	\else
		\write\@auxout{\string\wwwnotes@dlabel@store{#1}{#2}}%
	\fi}
% getdesc: 
%  #1 -- label, #2 -- filter, #3 -- error, #4 -- pre, #5 -- post
\newcommand\label@getdesc[5]{%
	\ifcsname www@label@store@description@#1\endcsname
		#2{#4\@nameuse{www@label@store@description@#1}#5}%
	\else
		#3%
	\fi}

% Showdesc tests if the label has already been shown on this page. If not, it 
% show the info for the label, and marks it as shown. The list of shown labels 
% is cleared at the end of every page with an shipout hook (see below)
\gdef\@desc@refs@on@page{}
\newcommand*\showdesc[1]{%
	\ifinlist{#1}{\@desc@refs@on@page}{\ClassInfo{wwwnotes2}{Label #1 is already shown, suppressing second display}}{%	
	\label@getdesc{#1}{\marginnote}{\ClassWarning{wwwnotes2}{Label #1 has no description}}{Ref.\ \ref*{#1}: ``}{''}%
\listgadd{\@desc@refs@on@page}{#1}}}

% Patch common reference commands. 
\newcommand*\desceqref[1]{\eqref{#1}\showdesc{#1}}
\newcommand*\descref[1]{\Cref{#1}\showdesc{#1}}

\AddToHook{shipout/before}{\gdef\@desc@refs@on@page{}}
%%%%% End Margin Notes and Descriptive Labelling %%%%%

