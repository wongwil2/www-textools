# wwwmath

Repackaging of the old `www_math_commands.sty` file.
See `wwwmath.sty` for list of included commands and hints on how they are to be used. 
