# msumathletter.cls

I recreated the standard Michigan State University Letter Head for the Department of Mathematics in LaTeX. 

A template:
````
\documentclass{msumathletter}
\begin{document}
\begin{letter}{}% Insert Name \\ address1\\address2\\ etc of recipient.
	\date{}% Insert a date if one other that today's date is desired.
	\opening{}% Insert something like, "Dear Colleague"
	% Type text of letter

	% Closing. Self explanatory
	\closing{\smash{\parbox[t]{0.55\textwidth}{Sincerely,\\
                                \hspace*{0.1in}\fromsig{\includegraphics[scale=0.3]{Signature.pdf}}\\[-0.27in]
		\fromname{Firstname Lastname\\ Associate Professor of Mathematics\\ Michigan State University}}}} 
\end{letter}
\end{document}
````

## Class options

* Standard options for the `letter` class are mostly available, in particular, `10pt`, `11pt`, and `12pt` are valid options, as well as papersize options.  
* This class uses [`wwwfontselect.sty`](../wwwfontselect/) to set fonts. Options recognized by it can also be passed to the class. 

## Usage Notes

### Addenda

For any addenda to the letter you probably want first to do `\newpage` to get a new page, and then `\startbreaks` to resume page breaking. See https://tex.stackexchange.com/questions/278029/

### Change margin

The default for this class sets the same margin for all pages. This can be a bit wasteful, as the first page includes a sidebar with the MSU Seal and Department Contact Info. Changing margins mid document is however non-trivial in TeX, because the `\newgeometry` will only kick in after end of paragraph and at pagebreak. The following is a step-by-step guide. 

1. First, make sure you are done with all other editing first, as this requires manual intervention concerning spacing. 
2. Locate the final _word_ on the first page. Find it in your TeX file. 
3. At the _start_ of the paragraph that contains said word, add `\begingroup\setlength\parfillskip{0pt}`. (Normally TeX allows the bottom line of a paragraph to have a gap to the end of line. This forces it to be full justified, creating the illusion that the paragraph break doesn't exist.)
4. Immediately _after_ the final word, add `\par\endgroup\newgeometry{margin=1in}\noindent`. (The `\par` ends the paragraph, the change in the previous line leaves it fully justified. `\endgroup` restore the normal end of paragraph glue. Use `\newgeometry` to set new margins, here I give the example of 1 inch on all sides. The `\noindent` ensure the following "paragraph" has no indentation, and so the illusion is complete.)

### 'thin' version

The standard version has fairly generous margins. For those days when you have to write a letter of reference with a page limit, `msumathletterthin.cls` is available. It introduces thinner margins. This necessarily makes the letterhead a bit uglier, as the Department Contact Info (for example) becomes more crammed.

## Copyright

The files `MSUheading.pdf` and `MSUSeal.pdf` are the Wordmark and Seal of Michigan State University; their copyright rests with the University, and their use here is under permission granted generally to faculty members for the use of University Logos when preparing official communications. 

The document class files themselves are released into public domain. 
