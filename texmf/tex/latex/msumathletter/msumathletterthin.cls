% MSU Math Dept letter class file

% Identification
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{msumathletterthin}[2023/09/27 v1.0 MSU Math Dept letter, WWYW version (thin)]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{letter}}
\ProcessOptions\relax

% Bring in the regular letter class
\LoadClass{letter}

% Bring in needed packages
\RequirePackage{ifthen}
\RequirePackage{graphicx}
\RequirePackage{wwwfontselect}
\RequirePackage{amsmath}
\RequirePackage[english]{babel}
\RequirePackage{microtype}
\RequirePackage[left=1.5in, right=1in, vmargin=1.05in]{geometry}
\RequirePackage{calc}
\RequirePackage[absolute]{textpos}

\renewcommand{\ps@firstpage}%
  {% 
  \let\@oddhead\@empty
  \let\@oddfoot\@empty
  % Place MSU head image
  \newgeometry{left=1.5in, right=1.05in, bottom=1.05in, top=1.5in}
  \begin{textblock*}{2.3in}[0,1](1.3in,1.5in)
	\includegraphics[width=2.3in]{MSUheading}
  \end{textblock*}

  % Place icon and Dept. return address in margin
  \begin{textblock*}{1.30in}[1,0](1.4in,\paperheight-5.5in)
  \flushright
  \includegraphics[width=0.8in]{MSUSealGreen}\\[12pt]
  {\fontsize{9}{11}\selectfont {\usefont{T1}{phv}{bc}{n} DEPARTMENT OF\\
  \usefont{T1}{phv}{bc}{n}MATHEMATICS}}\\[9pt]
  {\fontsize{8}{10}\selectfont {\usefont{T1}{phv}{m}{n}Michigan State University\\
      Wells Hall\\
       619 Red Cedar Road\\
       East Lansing, MI\\ 
  48824-1027\\ 
  Phone: 517-353-0844\\
  Fax: 517-432-1562}\\[7pt]
  \texttt{www.math.msu.edu}}
  \end{textblock*}
  
  \begin{textblock*}{1.3in}[1,0](1.4in,\paperheight-1in)
 \flushright 
 \fontsize{7}{9}\selectfont \usefont{T1}{phv}{m}{it} MSU is an affirmative-action/\\ equal opportunity employer.
  \end{textblock*}
} 

\let\oldps@plain\ps@plain
\def\ps@plain{%
	\oldps@plain
	\restoregeometry}
