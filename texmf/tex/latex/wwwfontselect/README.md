# wwwfontselect.sty

A font selection package; the purpose of this package is to make it easy to switch between different font pairings of text and math fonts for documents built using `pdftex`. To use: load

`\usepackage[options]{wwwfontselect}`

The default font is `kpfonts`. Valid options are

* `kpfonts`
* `cochineal` free clone of Crimson Pro, with math support through `newtxmath`
* `cpregular` and `cplight`, variants of the Crimson Pro fonts (these do not have math support)
* `coelacanth` also has no math support
* `libertinus`
* `newpx` for a Palladino clone
* `newtx` for a Times clone
* `noto`
