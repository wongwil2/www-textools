#  wwwsymb.sty

Code to get some whimsical symbols for LaTeX. 
This includes:

- A bootstrap symbol `\bootstrapSym{}` (mainly for use with denoting bootstrap assumptions).
- A lightning symbol `\energySym{}` (mainly for use with denoting energy inequalities).
- A prism-rainbow symbol `\dispersiveSym{}` (mainly for use with denoting dispersive estimates). 

The symbols have required arguments; leave empty if not need. 
The argument will be typeset as superscripts next to the symbol, with appropriate amount of kerning applied. 

See screenshot below. 

![Sample use image](Bootstrap-action.png)


