#!/bin/bash

for texfile in `git ls-tree master --name-only *tex`
do
	bname=`basename -s .tex $texfile`
	moddate=`git log -n 1 --date="format:%F %R" --format="format:This page last edited on %cd (\\texttt{%h})." ${texfile}`
	modcommit=`git log -n 1 --format="format:%h" ${texfile}`
	echo "\\def\\gitShortHash{$modcommit}" > ${bname}.gitinfo
	echo "\\def\\GitPageFooter{$moddate}" >> ${bname}.gitinfo
done
