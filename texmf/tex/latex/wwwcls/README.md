# Document Classes

In this directory you find a bunch of document classes I prepared for preparation of mathematical documents, especially lecture notes. 

## Getting `git` info

A lot of my classes have the capability to print `git` commit information in the footers.
This can be generated using the `makegitinfo.sh` shell script included in the repository. 
The script generates an auxiliary file for each `*.tex` file in the directory. 
By using `\wwwinclude` instead of `\include` those auxiliary files are read and the footer information updated. 

_Note:_ the shell script should be run _after_ you've committed your changes into the git repository, so that it will show the most updated information. 

## wwwlecture.cls

This is a newish iteration of my lecture notes class. 
The design is based on certain French math textbooks in which all "paragraphs" are numbered, this enables very clear cross referencing. 
Theorem-like environments are consider to be "special paragraphs", and share the same counter. 
Equations are numbered within each paragraph. 
Ample margins are allowed, in part to allow readers to take notes, in part so that the descriptive labelling / referencing commands can be used fruitfully. 

* Use `\lectureunit{<title>}{<dates>}` for general organization
     - (this is at chapter level), can be further grouped in \part
     - Section is not numbered
     - `\knowl` numbers "thought units", resets each chapter with the knowl counts shown in ToC per section. 
* Theorem-like environs:
       `thm`, `prop`, `lem`, `conj`, `defn`, `cor`, `openq`, `exa`, `exer`
       `proof`, `pfof`
* Extended referencing commands: use cleveref
* Descriptive labelling commands:
   `\desclabel`, `\descref`, `\desceqref`
* Margin note command:
   `\marginnote`, `\margincite`
     - Note: there is a bug in the marginfix package where sometimes when a marginpar 
     is called near a page break, that the marginpar gets silently dropped. A temp
     fix is to insert \clearmargin right before the note in question, so that the 
     marginpar buffer used by marginfix is cleared and the new marginpar is "forced 
     unto the next page" per se, with the "next page" being usually the correct page. 

