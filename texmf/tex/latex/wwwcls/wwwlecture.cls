%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  wwwlecture class by Willie W-Y Wong     
%%%%
%%%%  Document Structure Overview:
%%%%    Use \lectureunit{<title>}{<dates>} for general organization
%%%%           (this is at chapter level), can be further grouped in \part
%%%%        Section is not numbered
%%%%        \knowl   numbers "thought units", resets each chapter
%%%%            the knowl counts are shown in ToC per section. 
%%%%    Theorem-like environs:
%%%%        thm, prop, lem, conj, defn, cor, openq, exa, exer
%%%%        proof, pfof
%%%%  Extended referencing commands:
%%%%    Using cleveref
%%%%  Descriptive labelling commands:
%%%%    \desclabel, \descref, \desceqref
%%%%  Margin note command:
%%%%    \marginnote, \margincite
%%%%    Note: there is a bug in the marginfix package where sometimes when a marginpar 
%%%%      is called near a page break, that the marginpar gets silently dropped. A temp
%%%%      fix is to insert \clearmargin right before the note in question, so that the 
%%%%      marginpar buffer used by marginfix is cleared and the new marginpar is "forced 
%%%%      unto the next page" per se, with the "next page" being usually the correct page. 

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{wwwlecture}
	[2023/09/28 v0.5 Willie Wai Yeung Wong Lecture Notes class]

\LoadClass[10pt,titlepage,leqno,openright,oneside]{report}

\RequirePackage[T1]{fontenc}
\RequirePackage[english]{babel}
\RequirePackage{csquotes}

\RequirePackage{geometry}
\RequirePackage[pagestyles]{titlesec}
\RequirePackage{titletoc}
\RequirePackage{etoolbox}
\RequirePackage{chngcntr}
\RequirePackage{marginfix}
% marginfix sometimes loses notes completely? WTF?
\RequirePackage{titling}
\RequirePackage{moresize}

\RequirePackage{enumitem}
\RequirePackage[font=small,labelfont={sc,bf},margin={1.5em}]{caption}
\RequirePackage{imakeidx}
\RequirePackage[backend=biber,style=reading, entrykey=false, file=false,library=false, abstract=false]{biblatex}

\RequirePackage[oldstylenums,narrowiints%,intlimits,sumlimits
	,noDcommand]{kpfonts}
\RequirePackage[protrusion,babel]{microtype}

\RequirePackage[leqno]{mathtools, empheq}
\RequirePackage{amsthm}  % only using for qed commands

\RequirePackage[colorlinks=false]{hyperref}
\RequirePackage{cleveref}

%% Geometry (geometry)
\geometry{papersize={8.3in,11in},includemp=true,marginparwidth=1.44in,width=7in,bindingoffset=0.2in,hmarginratio={2:1},vmarginratio={3:4},headheight=14pt,headsep=0.2in,footskip=0.5in}

%% Git backup (gracefully degrades if user doesn't use Git)
\def\gitShortHash{(git not used)}
\def\GitPageFooter{(version info not available)}
\@input{\jobname.gitinfo}  % Inputs the .gitinfo file for the main document
\newcommand*\stashGitInfo{\let\old@gitSH\gitShortHash \let\old@GitPF\GitPageFooter}
\newcommand*\wwwinclude[1]{\begingroup\@input{#1.gitinfo}\include{#1}\endgroup}
\newcommand*\wwwinput[1]{\begingroup\@input{#1.gitinfo}\input{#1}\endgroup}


%% Title Page (titling, moresize)[hyperref]
\newcommand*\thesubtitle{}
\newcommand*\subtitle[1]{\renewcommand*\thesubtitle{#1}}
\NewCommandCopy\oldtitle\title
\newcommand*\thetitletext{}
\renewcommand*\title[1]{\renewcommand*\thetitletext{#1}\oldtitle{#1}}
\pretitle{\begin{flushleft}\HUGE \bfseries \scshape}
\posttitle{\par\end{flushleft}\begin{flushleft}\Large \thesubtitle\end{flushleft} \vfill}
\preauthor{\begin{flushright}\LARGE}
\postauthor{\par\end{flushright}%
	\hypersetup{pdfauthor={\theauthor},pdftitle={\thetitle},%
	pdfsubject={Mathematics}}%
	\vskip 1.5in}
\predate{\begin{center}\footnotesize Revision based on commit \texttt{\gitShortHash{}} as of }
\postdate{\par\end{center}}
\setlength{\droptitle}{0.5in}

%% Headers and Footers (titlesec, moresize)
\widenhead*{0.1in}{0.45in}
\newpagestyle{wwwfancy}{\headrule%
	\sethead[\thepage]%
		[][\scriptsize\slshape\thetitletext]%
		{\scriptsize\slshape\ifthechapter{\chaptertitlename~\thechapter.~\chaptertitle}{}}{}%
		{\thepage}%
	\setfoot{\tiny \copyright~\theauthor}{}{\tiny \GitPageFooter}}
\pagestyle{wwwfancy}

%%% Bibliography page style  (biblatex)
\defbibheading{bibintoc}[\bibname]{%
	\chapter*{#1}%
	\addcontentsline{toc}{chapter}{#1}
	\markboth{}{}
}
\newpagestyle{wwwbibfancy}{\headrule%
	\sethead[\thepage]%
		[][\slshape\thetitletext]%
		{\slshape \bibname}{}%
		{\thepage}%
	\setfoot{\tiny \copyright~\theauthor}{}{\tiny \GitPageFooter}}


%%% To make the clear page completely empty, with no header or footer
\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else\hbox{}
	%\vspace*{\fill}
	%\begin{center}
	%   This page intentially blank.
	%\end{center}
	%\vspace{\fill}
	\thispagestyle{empty}
	\newpage
	\if@twocolumn\hbox{}\newpage\fi\fi\fi}

%% Section/Chapter titles (titlesec, chngcntr, cleveref)
\setcounter{secnumdepth}{2}
\setcounter{tocdepth}{1}
\titleformat{\part}[display]{\bfseries\huge}%
	{\filcenter\partname\ \thepart}{0.4in}%
	{\filcenter\Huge}[\thispagestyle{empty}]
\newcommand*\chapterdate{}
\newcommand*\lectureunit[2]{%
	\renewcommand*\chapterdate{#2}%
	\chapter{#1}
}
\titleformat{\chapter}[display]{\bfseries\Large}%
	{\filleft\chaptertitlename\ {\huge\thechapter} {\large(\chapterdate)}}%
	{1ex}{\titlerule\vspace{2ex}\filcenter\Huge}%
	[\vspace{1ex}\titlerule\stepcounter{section}]  % Step the section counter to make the per section counter of knowls correct.
\titleformat{\section}{\bfseries\large}{}{0pt}{}
\renewcommand\thesubsection{\thechapter.\arabic{subsection}}
\newcommand\parenthesize@if@not@empty[1]{\if\relax\detokenize{#1}\relax\else\hspace{1ex}(#1)\fi}
\titleformat{\subsection}[runin]{\bfseries}{\thesubsection}{0pt}{}[\www@write@reg@subsects]
\titlespacing{\subsection}{0pt}{*3}{*2}
\counterwithout{subsection}{section}
\counterwithout{section}{chapter}
\counterwithin{subsection}{chapter}
\numberwithin{equation}{subsection}
\Crefname{subsection}{\P}{\P\P}
\crefname{subsection}{\P}{\P\P}
\newcommand\knowl[1][]{\subsection{\parenthesize@if@not@empty{#1}}}	
\Crefname{subsubappendix}{\P}{\P\P}
\crefname{subsubappendix}{\P}{\P\P}

%% Fix the fact that \appendix resets counters
\NewCommandCopy\oldappendix\appendix
\renewcommand\appendix{
	\newcounter{temp@subsect}
	\setcounter{temp@subsect}{\value{section}}
	\oldappendix
	\setcounter{section}{\value{temp@subsect}}
}

%%% Generate auxiliary counts
\newcommand\www@write@reg@subsects{\immediate\write\@auxout{\string\www@reg@subsects{\thesection}{\thesubsection}}}
\newcommand\www@reg@subsects[2]{%
	\ifcsname www@num@subsects@#1@start\endcsname
		\expandafter\gdef\csname www@num@subsects@#1\endcsname{\P\P\ \csname www@num@subsects@#1@start\endcsname -- #2}
	\else
		\expandafter\gdef\csname www@num@subsects@#1@start\endcsname{#2}
		\expandafter\gdef\csname www@num@subsects@#1\endcsname{\P #2}
	\fi}
	\newcommand\www@display@subsects[1]{\ifcsname www@num@subsects@#1\endcsname\csname www@num@subsects@#1\endcsname\fi}

%%% Section TOC (titletoc)
\renewcommand{\@pnumwidth}{2.8em}
\titlecontents{section}[1.8em]{}{}{}
{\hspace{1em}{\small [\www@display@subsects{\thecontentslabel}]}{\hspace{1ex}\titlerule*[1ex]{.}}\contentspage}


%%% Figure and Table numbering
\renewcommand\thefigure{\thechapter\Alph{figure}}
\renewcommand\thetable{\thechapter\Alph{table}}

%%%%% End basic set-up  %%%%%

%%%%% Theorem like environments (amsthm) %%%%%
\newenvironment{pfof}[1]%
	{\begin{proof}[Proof of \Cref{#1}]}%
	{\end{proof}}

%%% Custom Theorem Environments  (amsthm, cleveref)
%%%% Params:  theorem type, theorem name, qedsymbol, bodyfontspec
\newenvironment{statement}{}{}
\newcommand\www@propto@format@thm@header[2]{{\bfseries{\scshape #1}\parenthesize@if@not@empty{#2}}}

%%%% Params: Counter, name, qedsymbol, plural name, short name, bodyfontspec
\newcommand\www@create@thm[6]{%
	\newcounter{#1}
	\counterwithin{#1}{chapter}
	\expandafter\renewcommand\csname the#1\endcsname{\thechapter.\arabic{#1}}
	\Crefname{#1}{#2}{#4}
	\crefname{#1}{#5}{#5}
	\newenvironment{#1}[1][]{\setcounter{#1}{\value{subsection}}\renewcommand\qedsymbol{#3}\pushQED{\qed}\knowl\refstepcounter{#1}\www@propto@format@thm@header{#2}{##1}\par\nobreak\@afterheading\trivlist\item#6}{\popQED\endtrivlist\par}
	\newenvironment{#1@nonum}[1][]{\par\vspace{\parsep}\renewcommand\qedsymbol{#3}\pushQED{\qed}\noindent\www@propto@format@thm@header{#2}{##1}\par\nobreak\@afterheading\trivlist\item #6}{\popQED\endtrivlist}
	\newenvironment{#1*}[1][]{%
		\setcounter{#1}{\value{subsection}}
		\renewenvironment{statement}{\begin{#1@nonum}[##1]}{\end{#1@nonum}}%
		\knowl\refstepcounter{#1}}{\par}
}
\www@create@thm{thm}{Theorem}{$\blacksquare$}{Theorems}{Thm.}{\itshape}
\www@create@thm{defn}{Definition}{$\diamondsuit$}{Definitions}{Defn.}{\itshape}
\www@create@thm{cor}{Corollary}{$\blacksquare$}{Corollaries}{Cor.}{\itshape}
\www@create@thm{prop}{Proposition}{$\blacksquare$}{Propositions}{Prop.}{\itshape}
\www@create@thm{lem}{Lemma}{$\blacksquare$}{Lemmata}{Lem.}{\itshape}
\www@create@thm{conj}{Conjecture}{$\clubsuit$}{Conjectures}{Conj.}{\itshape}
\www@create@thm{openq}{Open Questions}{$\clubsuit$}{Open Questions}{Open Q.}{\itshape}

\www@create@thm{exa}{Example}{$\diamondsuit$}{Examples}{Ex.}{\normalfont}
\www@create@thm{exer}{Exercise}{$\spadesuit$}{Exercises}{Exer.}{\normalfont}

%%%%% End Theorem like environments %%%%%

%%%%% Margin Notes and Descriptive Labelling (amsmath) %%%%%
\newcommand*\marginfont{\footnotesize\itshape}
\setlength\marginparpush{9pt}
\newcommand{\marginnote}[1]{%
	\marginpar[\raggedleft\marginfont #1]{\raggedright\marginfont #1}}
\newcommand{\margincite}[1]{\marginnote{\cite{#1}}}

% The call to ifmeasuring below is due to AMS-math environments being called twice, first time to measure, the second time to print
% See e.g. http://tex.stackexchange.com/a/59088/119
% Store descriptions in the aux files so is robust against \includes
\newcommand\wwwnotes@dlabel@store[2]{\global\@namedef{www@label@store@description@#1}{#2}}

\newcommand\desclabel[2]{%
	\label{#1}%
	\ifmeasuring@
	\else
		\write\@auxout{\string\wwwnotes@dlabel@store{#1}{#2}}%
	\fi}
% getdesc: 
%  #1 -- label, #2 -- filter, #3 -- error, #4 -- pre, #5 -- post
\newcommand\label@getdesc[5]{%
	\ifcsname www@label@store@description@#1\endcsname
		#2{#4\@nameuse{www@label@store@description@#1}#5}%
	\else
		#3%
	\fi}

% Showdesc tests if the label has already been shown on this page. If not, it 
% show the info for the label, and marks it as shown. The list of shown labels 
% is cleared at the end of every page with an shipout hook (see below)
\gdef\@desc@refs@on@page{}
\newcommand*\showdesc[1]{%
	\ifinlist{#1}{\@desc@refs@on@page}{\ClassInfo{wwwnotes2}{Label #1 is already shown, suppressing second display}}{%	
	\label@getdesc{#1}{\marginnote}{\ClassWarning{wwwnotes2}{Label #1 has no description}}{\cref*{#1}: ``}{''}%
\listgadd{\@desc@refs@on@page}{#1}}}

% Patch common reference commands. 
\newcommand*\desceqref[1]{\eqref{#1}\showdesc{#1}}
\newcommand*\descref[1]{\Cref{#1}\showdesc{#1}}

\AddToHook{shipout/before}{\gdef\@desc@refs@on@page{}}
%%%%% End Margin Notes and Descriptive Labelling %%%%%

