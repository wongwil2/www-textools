%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  wwwslides class by Willie W-Y Wong     

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{wwwslides}
	[2023/11/29 v0.4 Willie Wai Yeung Wong Slides class]

\LoadClass[10pt,titlepage,leqno]{article}

\RequirePackage[T1]{fontenc}
\RequirePackage[english]{babel}
\RequirePackage{csquotes}

\RequirePackage{geometry}
\RequirePackage[pagestyles]{titlesec}
\RequirePackage{titling}
\RequirePackage{etoolbox,chngcntr}

\RequirePackage{enumitem}

\RequirePackage[leqno]{mathtools, empheq}
\RequirePackage{amsthm}  % only using for qed commands
\RequirePackage{gfsneohellenicot}
\RequirePackage[x11names]{xcolor}
\RequirePackage{pagecolor}

%% Geometry (geometry)
\geometry{papersize={4.8in,3.6in},hmargin=0.25in,vmargin={0.3in,0.45in}}

%% Colors (xcolor, pagecolor)
\pagecolor{DarkSeaGreen4!40!black}
\color{LavenderBlush2}
\newcommand*\HLo[1]{\begingroup\color{LightSalmon1}#1\endgroup}
\newcommand*\HLy[1]{\begingroup\color{LemonChiffon1}#1\endgroup}
\newcommand*\HLb[1]{\begingroup\color{Turquoise2}#1\endgroup}

%% Compactify all lists (enumitem)
\setlist{nosep, left=1ex}
\setlist[description]{style=standard,nosep,format=\normalfont\color{LemonChiffon1}\bfseries}

%% Title Page (titling)
\newcommand*\thesubtitle{}
\let\oldtitle\title
\newcommand*\thetitletext{}
\newcommand*\thevenuetext{}
\newcommand*\venue[1]{\renewcommand*\thevenuetext{#1}}
\renewcommand*\title[1]{\renewcommand*\thetitletext{#1}\oldtitle{#1}}
\newcommand*\shorttitle[1]{\renewcommand*\thetitletext{#1}}
\pretitle{\begin{flushleft}\LARGE \scshape}
\posttitle{\par\end{flushleft} \vfill}
\preauthor{\begin{flushright}\scriptsize}
\postauthor{\par\end{flushright}\vfill}
\predate{\begin{center}\scriptsize \thevenuetext\\ }
\postdate{\par\end{center}}
\setlength{\droptitle}{-0.7in}

%% Headers and Footers (titlesec, moresize)
\newpagestyle{wwwfancy}{\setfoot{\llap{\includegraphics[width=0.15in]{spartanhelmet.pdf}}}{\tiny\slshape\thetitletext}{\scriptsize\thepage}
	\sethead{}{}{}}
\pagestyle{wwwfancy}

%% General Styling
\setlength\parindent{0pt}

%% Section/Chapter titles (titlesec, chgcntr)
\setcounter{secnumdepth}{2}

\titleclass{\part}{page}
\assignpagestyle{\part}{empty}
\titleformat{\part}[display]{\normalfont\slshape}%
	{\filcenter\partname\ \thepart}{0.2in}%
	{\filcenter\normalfont\Large\scshape}
\titlespacing{\part}{0pt}{0.8in}{0pt}
\newcommand{\sectionbreak}{\clearpage} % So \section starts a new page
\titleformat{\section}{\large\scshape}{}{0pt}{}[{\titlerule}]
\renewcommand\thesubsection{\arabic{subsection}}
\counterwithout{subsection}{section}
\newcommand\parenthesize@if@not@empty[1]{\if\relax\detokenize{#1}\relax\else\hspace{1ex}(#1)\fi}
\titleformat{\subsection}[leftmargin]{\raggedleft\tiny}{\thesubsection}{0pt}{}
\titlespacing{\subsection}{0.2in}{*1}{0pt}
\titlespacing{\section}{0pt}{0pt}{*2}
\newcommand\knowl{\subsection{$\rangle$~~}}	

%%%%% End basic set-up  %%%%%

%%% Custom Theorem Environments  (amsthm)
%%%% Params:  theorem type, theorem name, qedsymbol, bodyfontspec
\newenvironment{statement}{}{}
\newcommand\www@propto@format@thm@header[2]{{\bfseries{\scshape #1}\parenthesize@if@not@empty{#2}}}

%%%% Params: Counter, name, qedsymbol, plural name, short name, bodyfontspec
\newcommand\www@create@thm[6]{%
	\newcounter{#1}
	\expandafter\renewcommand\csname the#1\endcsname{\arabic{#1}}
	\newenvironment{#1}[1][]{\setcounter{#1}{\value{subsection}}\renewcommand\qedsymbol{#3}\pushQED{\qed}\knowl\refstepcounter{#1}\www@propto@format@thm@header{#2}{##1}\par\nobreak\@afterheading\trivlist\item#6}{\popQED\endtrivlist\par}
	\newenvironment{#1@nonum}[1][]{\par\vspace{\parsep}\renewcommand\qedsymbol{#3}\pushQED{\qed}\noindent\www@propto@format@thm@header{#2}{##1}\par\nobreak\@afterheading\trivlist\item #6}{\popQED\endtrivlist}
	\newenvironment{#1*}[1][]{%
		\setcounter{#1}{\value{subsection}}
		\renewenvironment{statement}{\begin{#1@nonum}[##1]}{\end{#1@nonum}}%
		\knowl\refstepcounter{#1}}{\par}
}
\www@create@thm{thm}{Theorem}{$\mdlgblksquare$}{Theorems}{Thm.}{\itshape}
\www@create@thm{defn}{Definition}{$\diamondsuit$}{Definitions}{Defn.}{\itshape}
\www@create@thm{cor}{Corollary}{$\mdlgblksquare$}{Corollaries}{Cor.}{\itshape}
\www@create@thm{prop}{Proposition}{$\mdlgblksquare$}{Propositions}{Prop.}{\itshape}
\www@create@thm{lem}{Lemma}{$\mdlgblksquare$}{Lemmata}{Lem.}{\itshape}
\www@create@thm{conj}{Conjecture}{$\clubsuit$}{Conjectures}{Conj.}{\itshape}
\www@create@thm{openq}{Open Questions}{$\clubsuit$}{Open Questions}{Open Q.}{\itshape}

\www@create@thm{exa}{Example}{$\diamondsuit$}{Examples}{Ex.}{\normalfont}
\www@create@thm{exer}{Exercise}{$\spadesuit$}{Exercises}{Exer.}{\normalfont}
\www@create@thm{rmk}{Remark}{$\diamondsuit$}{Remarks}{Rmk.}{\normalfont}

%%%%% End Theorem like environments %%%%%

%%%%% Math Symbols %%%%%
\AtBeginDocument{\renewcommand*\Box{\mdlgwhtsquare}}
\AtBeginDocument{\renewcommand*\qedsymbol{$\laplac$}}
