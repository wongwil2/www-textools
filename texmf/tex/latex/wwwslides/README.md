# wwwslides.cls

A minimal slides class with a Michigan State University inspired theme (including a Spartan Helmet logo) that is off-white on dark green, almost like a black board. 

This class uses the GFS NeoHellenic font, originally designed for presentations; however, this means it can only be built using XeTeX or LuaTeX (not PDFtex). 


## Example Document

````
\documentclass{wwwslides}
\usepackage{multicol}
\setlength{\multicolsep}{0pt}

\begin{document}
\title{Test Title}
\author{Willie Wong\\ Michigan State University\\ \texttt{wongwwy@math.msu.edu}}
\venue{Seminar Name}

\maketitle

\part{Test}

\section{First slide}

Some text

\begin{multicols}{2}[\section{Section name}]
\knowl test text

\begin{itemize}
	\item test
	\item Another test
\end{itemize}
\[ E = mc^2 \]
\knowl Test test
\end{multicols}
\knowl Another test

\end{document}
````

## Features

### Title Page

Use:

* `\title` as normal
* `\author` as normal, can insert line breaks with `\\` to show affiliation information etc. 
* `\date` to set talk date
* `\venue` to set event venue
* `\maketitle` will generate a title slide

### Structuring and Pagination

* `\part{<part title>}` creates single slide with part title
* `\section{<heading>}` starts **new slide** with given section heading
* within each slide, vertical spacing can be adjusted using `\vfill`
* Compatible with `multicol` package

### Content and Numbered Paragraphs

* Three highlight colors are pre-defined via `\HLo{material}` (orange) `\HLy{material}` (yellow) and `\HLb{material}` (blue)
* Start numbered paragraphs with `\knowl`
* Theorem-like environments (numbered together with paragraphs): `thm`, `defn`, `cor`, `prop`, `lem`, `conj`, `openq`, `exa`, `exer`, `rmk`
* Description lists have highlighted main entries
